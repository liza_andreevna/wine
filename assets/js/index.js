$(function () {
    setInterval("rotateImages()", 3000);
});

function rotateImages() {
    let current = $(".current");
    let next = current.next();
    current.parent().height(current.height());
    if (next.length == 0) next = $(".hero-slider div:first");
    current.removeClass('current').addClass('prev');
    next.css({opacity: 0}).addClass('current').animate({opacity: 1}, 1000,
        function () {
            current.removeClass('prev');
        }
    )
}
